# -*- coding: utf-8 -*
"""
    @filename: GCrawler.py
    @author: Mark Chang 
    @date: 2014/3/12
    
    Efficient Crawler

"""
import codecs
def open_and_write(fname,w):
    f=codecs.open(fname,'w','utf-8')
    f.write(w)
    f.close()

def open_and_read(fname,by_codecs=True):
    f=None
    if by_codecs: 
        f=codecs.open(fname,'r','utf-8')
    else :
        f=open(fname,'r')
    lines=f.readlines()
    f.close()
    return lines


def get_key_val(dicti,key,empty):
    if key not in dicti.keys():
        return copy.copy(empty)
    else:
        return dicti[key] 

def check_key(self,dicti,keys):
    if type(keys) is list:
        for key in keys:
            if key not in dicti.keys():
                return False
    else:
        if keys not in dicti.keys():
            return False
    return True



def isin_list_dict(dic_list,key,val):
    for dic in dic_list:
        if key in dic.keys():
            if val==dic[key]:
                return True
    return False 

def search_list_dict(dic_list, key, val):
    for dic in dic_list:
        if key in dic.keys():
            return key[val]
    return None 

def get_tlist_data(tuple_list, key):
    for (x,y) in tuple_list:
        if x==key:
            return y
    return None

def search_tlist_data(tuple_list, key,val):
    for (x,y) in tuple_list:
        if key in x and val in y:
            return (x,y)
    return None

def print_tlist_data(tuple_list):
    for (x,y) in tuple_list: 
        print x,y

