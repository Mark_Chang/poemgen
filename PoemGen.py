# -*- coding:utf-8 -*-
from util import *
from MyPrinter import MyPrinter
import nltk
import json
from random import random,seed
from sys import argv
_START_TAG = '<start>'
_END_TAG = '<end>'
_END_TYPE = [s.encode('utf-8') for s in [u"。",u"，"]]
_input_raw = json.loads("".join(open_and_read("poem300.json")))
_input_text_end = [" ".join([s.encode('utf-8') for s in s2]) for s2 in _input_raw ]
_input_text_noend = [" ".join([s.encode('utf-8') for s in s2 if s.encode('utf-8') not in _END_TYPE ]) for s2 in _input_raw ]
#input_text_un = [" ".join([s for s in s2]) for s2 in input_text ]
_gram_end={1:{},2:{},3:{}}
_gram_noend={1:{},2:{},3:{}}

def gen_gram(itext,n):
    itext_ary = itext.split(' ')
    #if n>=2 :
    #    itext_ary = [_START_TAG]+itext_ary+[_END_TAG]
    return  [ " ".join(itext_ary[s:s+n]) for s in range(len(itext_ary)+1-n) ]
 
def calculage_gram():
    for sent in _input_text_end:
        for i in _gram_end.keys():
            for gram in gen_gram(sent,i):    
               _gram_end[i][gram]=_gram_end[i].get(gram,0)+1
    for sent in _input_text_noend:
        for i in _gram_noend.keys():
            for gram in gen_gram(sent,i):    
               _gram_noend[i][gram]=_gram_noend[i].get(gram,0)+1
def get_bigram(word):
    bigram_list = sorted([(key,_gram_noend[2][key]) for key in _gram_noend[2].keys() if word == key.split()[0] ]
                    ,key=lambda x:x[1]
                    ,reverse=True)
    if len(bigram_list) > 0 :
        result = bigram_list[int(random()*len(bigram_list)*0.3)]
    else:
        unigram_list = sorted(_gram_noend[1].items()
                    ,key=lambda x:x[1]
                    ,reverse=True)
        result = unigram_list[int(random()*len(unigram_list))]
    return result[0]
    
      
def gen_sentence(word,length):
    start = word.encode('utf-8')  
    result =[start]
    for i in range(length):
        bg = get_bigram(start).split()
        #print bg[0],bg[1]
        if i > 0:
            result.append(bg[0])        
        if len(bg) > 1:
            start = bg[1]
        else:
            start = bg
    return "".join(result)

def gen_poem(input_str,length):
    for s in input_str:
       #print s
       print gen_sentence(s,length) 

#def get_

#seed( 10 )
calculage_gram()
gen_poem(argv[1].decode('utf-8') ,5)

        
